﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private GameStateChanger _pcTurnGameStateChanger;

    private bool _playerTurn = false;
    private MidStackController _midStackController;

    private void Awake()
    {
        GameStateManager.Instance.OnGameStateChanged += HandleGameStateChanged;
        _midStackController = MidStackController.Instance;
    }

    private void FixedUpdate()
    {
        if (!_playerTurn)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                CardObject card = hit.transform.GetComponent<CardObject>();
                if (card != null)
                {
                    if (card.isPlayerCard)
                    {
                        card.TargetPosition = _midStackController.GetPositions(1)[0];
                        _midStackController.PushToStack(card);

                        _pcTurnGameStateChanger.ChangeGameState();
                    }
                }
            }
                
        }
    }

    private void OnDestroy()
    {
        if (GameStateManager.Instance != null)
            GameStateManager.Instance.OnGameStateChanged -= HandleGameStateChanged;
    }

    private void HandleGameStateChanged(GameState newState)
    {
        _playerTurn = newState == GameState.PlayerTurn;
    }
}
