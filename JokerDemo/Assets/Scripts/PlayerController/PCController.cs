﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCController : MonoBehaviour
{
    [SerializeField]
    private GameStateChanger _playerTurnGameStateChanger;

    private List<CardObject> _pcHand = new List<CardObject>();
    private MidStackController _midStackController;
    
    private void Awake()
    {
        GameStateManager.Instance.OnGameStateChanged += HandleGameStateChanged;
        _midStackController = MidStackController.Instance;
    }

    private void OnDestroy()
    {
        if (GameStateManager.Instance != null)
            GameStateManager.Instance.OnGameStateChanged -= HandleGameStateChanged;
    }

    private void HandleGameStateChanged(GameState newState)
    {
        if (newState == GameState.PCTurn)
        {
            ChooseCard();
        }
    }

    private void ChooseCard()
    {
        CardObject cardToPlay;
        Card topCard = _midStackController.GetTopCardInfo();

        //Search for a same valued card
        CardObject card = _pcHand.Find((c) => c.CardData.value == topCard.value);
        if (card != null)
        {
            Debug.Log("got a same valued card..");
            cardToPlay = card;

            _pcHand.Remove(cardToPlay);

            StartCoroutine(PlayCard(cardToPlay));

            return;
        }

        //Search for a jack
        card = _pcHand.Find((c) => c.CardData.value == CardValue.Jack);
        if (card != null)
        {
            Debug.Log("got a jack..");
            cardToPlay = card;

            _pcHand.Remove(cardToPlay);

            StartCoroutine(PlayCard(cardToPlay));

            return;
        }

        //No jack or no same valued card, just play a random one
        Debug.Log("random card..");
        cardToPlay = _pcHand[Random.Range(0, (_pcHand.Count - 1))];

        _pcHand.Remove(cardToPlay);

        StartCoroutine(PlayCard(cardToPlay));
    }

    private IEnumerator PlayCard(CardObject cardToPlay)
    {
        yield return new WaitForSecondsRealtime(1f);

        cardToPlay.IsCardOpen = true;
        cardToPlay.TargetPosition = _midStackController.GetPositions(1)[0];
        _midStackController.PushToStack(cardToPlay);

        _playerTurnGameStateChanger.ChangeGameState();
    }

    public void AddToHand(List<CardObject> cards)
    {
        _pcHand.AddRange(cards);
    }
}
