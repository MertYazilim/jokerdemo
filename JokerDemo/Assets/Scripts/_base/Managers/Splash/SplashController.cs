﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class SplashController : MonoBehaviour {

    public string sceneName;
    public CanvasGroup canvasGroup;
    public float splashDuration = 1.0f;
    public float fadeMultiplier = 0.8f;
    
    private IEnumerator Start() {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
		
        yield return new WaitForSeconds(splashDuration);

		ChangeScene();
    }

    private void ChangeScene() {

        SceneManager.UnloadSceneAsync("Splash");
        
    }

}
