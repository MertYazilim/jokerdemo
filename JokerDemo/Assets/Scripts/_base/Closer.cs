﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Closer : MonoBehaviour
{

    RectTransform myRect;

    private void Awake() {
        myRect = transform as RectTransform;
        gameObject.SetActive(false);
    }

    void Update() {

        if (Input.GetMouseButtonDown(0)) {

            Vector2 clickPos = Input.mousePosition;

            if (!RectTransformUtility.RectangleContainsScreenPoint(myRect, clickPos, null))
                gameObject.SetActive(false);

        }


    }

}
