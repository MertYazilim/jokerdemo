// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VRM/ProcedurelReveal"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Control("Control", Float) = 0.25
		_EmissiveAreaLengh("EmissiveAreaLengh", Range( 0 , 1)) = 0
		[HDR]_EmissiveColor("EmissiveColor", Color) = (0,0,0,0)
		_Albedo("Albedo", Color) = (0,0,0,0)
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[Toggle]_x("x", Float) = 0
		[Toggle]_y("y", Float) = 1
		[Toggle]_z("z", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 2.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			half2 uv_texcoord;
			float3 worldPos;
		};

		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform half4 _Albedo;
		uniform half _x;
		uniform half _y;
		uniform half _z;
		uniform half _Control;
		uniform half _EmissiveAreaLengh;
		uniform half4 _EmissiveColor;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			o.Albedo = ( tex2D( _TextureSample0, uv_TextureSample0 ) * _Albedo ).rgb;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float temp_output_35_0 = ( lerp(0.0,(ase_vertex3Pos).x,_x) + lerp(0.0,(ase_vertex3Pos).y,_y) + lerp(0.0,(ase_vertex3Pos).z,_z) );
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float temp_output_26_0 = ( ase_objectScale.y * _Control );
			float temp_output_2_0 = step( temp_output_35_0 , temp_output_26_0 );
			o.Emission = ( ( temp_output_2_0 - step( ( temp_output_35_0 + _EmissiveAreaLengh ) , temp_output_26_0 ) ) * _EmissiveColor ).rgb;
			o.Alpha = 1;
			clip( temp_output_2_0 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16100
0;71;1437;958;1886.453;1093.759;1.652387;True;True
Node;AmplifyShaderEditor.PosVertexDataNode;32;-894.0764,-691.0117;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;15;-574.6823,-712.4943;Float;False;True;False;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;37;-592.0364,-592.841;Float;False;False;True;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;39;-590.1569,-489.4583;Float;False;False;False;True;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;33;-318.2403,-730.1426;Half;False;Property;_x;x;6;0;Create;False;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;36;-335.5945,-610.4893;Float;False;Property;_y;y;7;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;38;-333.715,-507.1066;Float;False;Property;_z;z;8;0;Create;True;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-797.5,125;Half;False;Property;_EmissiveAreaLengh;EmissiveAreaLengh;2;0;Create;True;0;0;False;0;0;0.241;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-789.1846,-107.0817;Half;False;Property;_Control;Control;1;0;Create;True;0;0;False;0;0.25;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ObjectScaleNode;25;-793.0972,-257.5933;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;35;-63.44301,-614.823;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-517.7919,-156.8177;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;5;-424.3146,183.0148;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;2;-196.5,-108;Float;True;2;0;FLOAT;0;False;1;FLOAT;0.53;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;4;-190.5,126;Float;True;2;0;FLOAT;0;False;1;FLOAT;0.53;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;19.74085,-422.046;Half;False;Property;_Albedo;Albedo;4;0;Create;True;0;0;False;0;0,0,0,0;0,0.01192939,0.4622642,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;9;54.5,-239;Half;False;Property;_EmissiveColor;EmissiveColor;3;1;[HDR];Create;True;0;0;False;0;0,0,0,0;6.457122,0.5100135,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;7;44.5,-35;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;30;262.3069,-542.3083;Float;True;Property;_TextureSample0;Texture Sample 0;5;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;617.9846,-408.245;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldPosInputsNode;23;-1353.342,-565.6622;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;275.5,-50;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;22;-1349.742,-409.6501;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;24;-1119.694,-486.9877;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;500.5668,-199.7611;Half;False;True;0;Half;ASEMaterialInspector;0;0;Standard;VRM/ProcedurelReveal;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;15;0;32;0
WireConnection;37;0;32;0
WireConnection;39;0;32;0
WireConnection;33;1;15;0
WireConnection;36;1;37;0
WireConnection;38;1;39;0
WireConnection;35;0;33;0
WireConnection;35;1;36;0
WireConnection;35;2;38;0
WireConnection;26;0;25;2
WireConnection;26;1;3;0
WireConnection;5;0;35;0
WireConnection;5;1;6;0
WireConnection;2;0;35;0
WireConnection;2;1;26;0
WireConnection;4;0;5;0
WireConnection;4;1;26;0
WireConnection;7;0;2;0
WireConnection;7;1;4;0
WireConnection;31;0;30;0
WireConnection;31;1;11;0
WireConnection;10;0;7;0
WireConnection;10;1;9;0
WireConnection;24;0;23;0
WireConnection;24;1;22;0
WireConnection;0;0;31;0
WireConnection;0;2;10;0
WireConnection;0;10;2;0
ASEEND*/
//CHKSM=AC6608B8832DBAC1EDEA6AB62C2776E5B2C84947