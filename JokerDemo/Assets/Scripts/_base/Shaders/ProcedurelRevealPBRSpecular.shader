// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VRM/ProcedurelRevealPBRSpecular"
{
	Properties
	{
		_Control("Control", Float) = 0.25
		_EmissiveAreaLengh("EmissiveAreaLengh", Range( 0 , 1)) = 0
		[HDR]_EmissiveColor("EmissiveColor", Color) = (0,0,0,0)
		[Toggle]_x("x", Float) = 0
		[Toggle]_y("y", Float) = 1
		[Toggle]_z("z", Float) = 0
		_Color("Color", Color) = (0,0,0,0)
		_MainTex("MainTex", 2D) = "white" {}
		_BumpMap("BumpMap", 2D) = "bump" {}
		_BumpScale("BumpScale", Float) = 0
		_specColor("specColor", Color) = (0,0,0,0)
		_Specular("Specular", 2D) = "black" {}
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#pragma target 2.0
		#pragma surface surf StandardSpecular keepalpha addshadow fullforwardshadows 
		struct Input
		{
			half2 uv_texcoord;
			float3 worldPos;
		};

		uniform sampler2D _BumpMap;
		uniform float4 _BumpMap_ST;
		uniform half _BumpScale;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform half4 _Color;
		uniform half _x;
		uniform half _y;
		uniform half _z;
		uniform half _Control;
		uniform half _EmissiveAreaLengh;
		uniform half4 _EmissiveColor;
		uniform sampler2D _Specular;
		uniform float4 _Specular_ST;
		uniform half4 _specColor;
		uniform half _Smoothness;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv_BumpMap = i.uv_texcoord * _BumpMap_ST.xy + _BumpMap_ST.zw;
			o.Normal = ( UnpackScaleNormal( tex2D( _BumpMap, uv_BumpMap ), 0.0 ) * _BumpScale );
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			o.Albedo = ( tex2D( _MainTex, uv_MainTex ) * _Color ).rgb;
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float temp_output_35_0 = ( lerp(0.0,(ase_vertex3Pos).x,_x) + lerp(0.0,(ase_vertex3Pos).y,_y) + lerp(0.0,(ase_vertex3Pos).z,_z) );
			float3 ase_objectScale = float3( length( unity_ObjectToWorld[ 0 ].xyz ), length( unity_ObjectToWorld[ 1 ].xyz ), length( unity_ObjectToWorld[ 2 ].xyz ) );
			float temp_output_26_0 = ( ase_objectScale.y * _Control );
			float temp_output_2_0 = step( temp_output_35_0 , temp_output_26_0 );
			o.Emission = ( ( temp_output_2_0 - step( ( temp_output_35_0 + _EmissiveAreaLengh ) , temp_output_26_0 ) ) * _EmissiveColor ).rgb;
			float2 uv_Specular = i.uv_texcoord * _Specular_ST.xy + _Specular_ST.zw;
			o.Specular = ( tex2D( _Specular, uv_Specular ) * _specColor ).rgb;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
			clip( temp_output_2_0 - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=16100
1;562;1512;445;3036.147;919.0162;4.129893;True;True
Node;AmplifyShaderEditor.PosVertexDataNode;32;-894.0764,-691.0117;Float;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;15;-574.6823,-712.4943;Float;False;True;False;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;37;-592.0364,-592.841;Float;False;False;True;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;39;-590.1569,-489.4583;Float;False;False;False;True;False;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;33;-318.2403,-730.1426;Half;False;Property;_x;x;3;0;Create;False;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;36;-335.5945,-610.4893;Float;False;Property;_y;y;4;0;Create;True;0;0;False;0;1;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ToggleSwitchNode;38;-333.715,-507.1066;Float;False;Property;_z;z;5;0;Create;True;0;0;False;0;0;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;6;-797.5,125;Half;False;Property;_EmissiveAreaLengh;EmissiveAreaLengh;1;0;Create;True;0;0;False;0;0;0.154;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-789.1846,-107.0817;Half;False;Property;_Control;Control;0;0;Create;True;0;0;False;0;0.25;-1.41;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ObjectScaleNode;25;-793.0972,-257.5933;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;35;-63.44301,-614.823;Float;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-517.7919,-156.8177;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;5;-424.3146,183.0148;Float;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;2;-196.5,-108;Float;True;2;0;FLOAT;0;False;1;FLOAT;0.53;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;4;-190.5,126;Float;True;2;0;FLOAT;0;False;1;FLOAT;0.53;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;30;263.9593,-727.3757;Float;True;Property;_MainTex;MainTex;7;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;41;756.3295,-649.968;Float;True;Property;_BumpMap;BumpMap;8;0;Create;True;0;0;False;0;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;44;1146.665,-442.5714;Half;False;Property;_BumpScale;BumpScale;9;0;Create;True;0;0;False;0;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;11;19.74085,-422.046;Half;False;Property;_Color;Color;6;0;Create;True;0;0;False;0;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;46;135.5887,548.9473;Float;False;Property;_specColor;specColor;10;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;7;44.5,-35;Float;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;9;54.5,-239;Half;False;Property;_EmissiveColor;EmissiveColor;2;1;[HDR];Create;True;0;0;False;0;0,0,0,0;0,0.6478755,2.670157,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;40;90.38663,236.2687;Float;True;Property;_Specular;Specular;11;0;Create;True;0;0;False;0;None;None;True;0;False;black;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;42;833.9871,-228.3566;Float;False;Property;_Smoothness;Smoothness;12;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ObjectToWorldTransfNode;22;-1349.742,-409.6501;Float;False;1;0;FLOAT4;0,0,0,1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;534.543,388.5833;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;24;-1119.694,-486.9877;Float;False;2;0;FLOAT3;0,0,0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;543.6274,-495.8213;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;275.5,-50;Float;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;43;1271.827,-565.7781;Float;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;23;-1353.342,-565.6622;Float;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;884.9222,159.9765;Half;False;True;0;Half;ASEMaterialInspector;0;0;StandardSpecular;VRM/ProcedurelRevealPBRSpecular;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;13;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;15;0;32;0
WireConnection;37;0;32;0
WireConnection;39;0;32;0
WireConnection;33;1;15;0
WireConnection;36;1;37;0
WireConnection;38;1;39;0
WireConnection;35;0;33;0
WireConnection;35;1;36;0
WireConnection;35;2;38;0
WireConnection;26;0;25;2
WireConnection;26;1;3;0
WireConnection;5;0;35;0
WireConnection;5;1;6;0
WireConnection;2;0;35;0
WireConnection;2;1;26;0
WireConnection;4;0;5;0
WireConnection;4;1;26;0
WireConnection;7;0;2;0
WireConnection;7;1;4;0
WireConnection;45;0;40;0
WireConnection;45;1;46;0
WireConnection;24;0;23;0
WireConnection;24;1;22;0
WireConnection;31;0;30;0
WireConnection;31;1;11;0
WireConnection;10;0;7;0
WireConnection;10;1;9;0
WireConnection;43;0;41;0
WireConnection;43;1;44;0
WireConnection;0;0;31;0
WireConnection;0;1;43;0
WireConnection;0;2;10;0
WireConnection;0;3;45;0
WireConnection;0;4;42;0
WireConnection;0;10;2;0
ASEEND*/
//CHKSM=A4F303521E79353F2F7A75788ADB31A4C1B98D97