// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UIScanShader"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		[PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
		_Speed("Speed", Float) = 0.2
		_Float1("Float 1", Float) = 0.05
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
			
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		
		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				float2 texcoord  : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				float4 ase_texcoord1 : TEXCOORD1;
			};
			
			uniform fixed4 _Color;
			uniform float _EnableExternalAlpha;
			uniform sampler2D _MainTex;
			uniform sampler2D _AlphaTex;
			uniform sampler2D _TextureSample0;
			uniform float4 _TextureSample0_ST;
			uniform float _Speed;
			uniform float _Float1;
			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				float4 clipPos = UnityObjectToClipPos(IN.vertex);
				float4 screenPos = ComputeScreenPos(clipPos);
				OUT.ase_texcoord1 = screenPos;
				
				
				IN.vertex.xyz +=  float3(0,0,0) ; 
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			fixed4 SampleSpriteTexture (float2 uv)
			{
				fixed4 color = tex2D (_MainTex, uv);

#if ETC1_EXTERNAL_ALPHA
				// get the color from an external texture (usecase: Alpha support for ETC1 on android)
				fixed4 alpha = tex2D (_AlphaTex, uv);
				color.a = lerp (color.a, alpha.r, _EnableExternalAlpha);
#endif //ETC1_EXTERNAL_ALPHA

				return color;
			}
			
			fixed4 frag(v2f IN  ) : SV_Target
			{
				float2 uv_TextureSample0 = IN.texcoord.xy * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
				float4 tex2DNode21 = tex2D( _TextureSample0, uv_TextureSample0 );
				float4 screenPos = IN.ase_texcoord1;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float temp_output_9_0 = fmod( ( _Time.y * _Speed ) , 0.15 );
				float temp_output_11_0 = fmod( ( fmod( abs( _SinTime.w ) , 1.0 ) + ase_screenPosNorm.y ) , 1.0 );
				float ifLocalVar16 = 0;
				if( fmod( ase_screenPosNorm.y , temp_output_9_0 ) >= ( _Float1 * temp_output_9_0 ) )
				ifLocalVar16 = temp_output_11_0;
				else
				ifLocalVar16 = 1.0;
				float4 appendResult19 = (float4(tex2DNode21.r , tex2DNode21.g , tex2DNode21.b , ( tex2DNode21.a * ifLocalVar16 )));
				
				fixed4 c = appendResult19;
				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
546;102;1351;745;1420.365;499.3657;1.422063;True;False
Node;AmplifyShaderEditor.SinTimeNode;1;-930.8738,-374.5291;Float;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;3;-1156.928,-355.6717;Float;False;1;0;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.AbsOpNode;4;-754.1446,-293.1385;Float;False;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;2;-1325.668,-244.9072;Float;False;Property;_Speed;Speed;1;0;Create;0.2;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-1071.695,-245.4227;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FmodOpNode;5;-876.2749,-134.3977;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;7;-1169.704,-44.89459;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;10;-829.0923,111.2937;Float;False;Property;_Float1;Float 1;2;0;Create;0.05;0.02;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FmodOpNode;9;-815.9496,366.3217;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.15;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;8;-540.6763,-169.4259;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-324.6703,362.3338;Float;False;Constant;_Float2;Float 2;3;0;Create;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-517.8895,144.1677;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FmodOpNode;12;-576.8783,-22.95218;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FmodOpNode;11;-393.407,-148.516;Float;False;2;0;FLOAT;0.0;False;1;FLOAT;1.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;21;-415.3194,-412.2214;Float;True;Property;_TextureSample0;Texture Sample 0;3;0;Create;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;16;-247.2229,11.2776;Float;False;False;5;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-55.18369,-54.25981;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosFromTransformMatrix;22;-93.58764,278.2442;Float;False;1;0;FLOAT4x4;0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;15;-1165.263,220.4885;Float;False;Property;_Float3;Float 3;0;0;Create;0;0.11;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;19;173.5091,-98.59583;Float;False;FLOAT4;4;0;FLOAT;0.0;False;1;FLOAT;0.0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMasterNode;20;337,46;Float;False;True;2;Float;ASEMaterialInspector;0;4;UIScanShader;0f8ba0101102bb14ebf021ddadce9b49;Sprites Default;3;One;OneMinusSrcAlpha;0;One;Zero;Off;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;0
WireConnection;4;0;1;4
WireConnection;6;0;3;0
WireConnection;6;1;2;0
WireConnection;5;0;4;0
WireConnection;9;0;6;0
WireConnection;8;0;5;0
WireConnection;8;1;7;2
WireConnection;13;0;10;0
WireConnection;13;1;9;0
WireConnection;12;0;7;2
WireConnection;12;1;9;0
WireConnection;11;0;8;0
WireConnection;16;0;12;0
WireConnection;16;1;13;0
WireConnection;16;2;11;0
WireConnection;16;3;11;0
WireConnection;16;4;14;0
WireConnection;23;0;21;4
WireConnection;23;1;16;0
WireConnection;19;0;21;1
WireConnection;19;1;21;2
WireConnection;19;2;21;3
WireConnection;19;3;23;0
WireConnection;20;0;19;0
ASEEND*/
//CHKSM=D905220DA896044931043B7C80DCC4C2DEA3C5A3