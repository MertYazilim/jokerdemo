﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public static class Extensions {

    public static T ParseEnum<T>(this string value, T defaultValue) where T : struct, IConvertible {
        if (!typeof(T).IsEnum) throw new ArgumentException("ParseEnum<T> : T must be an enum! Given Type : " + typeof(T).ToString());
        if (string.IsNullOrEmpty(value)) {
            return defaultValue;
        }

        string myValue = value.Trim();
        foreach (T item in Enum.GetValues(typeof(T))) {
            if (item.ToString().Equals(myValue)) {
                return item;
            }
        }

        return defaultValue;
    }
}
