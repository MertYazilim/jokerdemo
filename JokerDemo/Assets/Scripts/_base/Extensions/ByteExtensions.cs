﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public static class ByteExtensions{

    public static T Deserialize<T>(this byte[] bytes) {
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
        MemoryStream ms = new MemoryStream();
        ms.Write(bytes, 0, bytes.Length);
        ms.Seek(0, SeekOrigin.Begin);
        return (T)xmlSerializer.Deserialize(ms);
    }
}
