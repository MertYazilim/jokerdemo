﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions {

    
    public static void ClearChilds(this Transform transform) {
        
        while(transform.childCount > 0) {
            GameObject child = transform.GetChild(0).gameObject;
#if UNITY_EDITOR
            if (Application.isEditor) {
                GameObject.DestroyImmediate(child);
            }
            else
#endif
            {
                GameObject.Destroy(child);
            }

        }
    }

    public static int GetChildId(this Transform transform) {

        Transform parent = transform.parent;
        if (parent == null)
            return -1;

        for(int i = 0; i < parent.childCount;i++) {
            if (parent.GetChild(i) == transform)
                return i;
        }

        return -1;
    }

    public static Transform EnsureChild(this Transform transform, string childName) {

        Transform child = transform.Find(childName);
        if (child == null) {
            child = new GameObject(childName).transform;
        }
        child.SetParent(transform, false);
        return child;

    }

    public static void SetActiveAllChild(this Transform transform) {
        if (!transform.gameObject.activeSelf)
            transform.gameObject.SetActive(true);

        for (int i = 0; i < transform.childCount; i++)
            transform.GetChild(i).SetActiveAllChild();
    }

    public static void Destroy(this Transform go) {

        if (go != null) {
            go.gameObject.Destroy();
        }


    }

    public static void Billboard(this Transform transform, PivotAxis pivotAxis, Transform target, bool inverse = false) {

        // Get a Vector that points from the target to the main camera.
        Vector3 directionToTarget = inverse ? transform.position - target.position : target.position - transform.position;
        Vector3 targetUpVector = target.up;

        // Adjust for the pivot axis.
        switch (pivotAxis) {
            case PivotAxis.X:
                directionToTarget.x = 0.0f;
                targetUpVector = Vector3.up;
                break;

            case PivotAxis.Y:
                directionToTarget.y = 0.0f;
                targetUpVector = Vector3.up;
                break;

            case PivotAxis.Z:
                directionToTarget.x = 0.0f;
                directionToTarget.y = 0.0f;
                break;

            case PivotAxis.XY:
                targetUpVector = Vector3.up;
                break;

            case PivotAxis.XZ:
                directionToTarget.x = 0.0f;
                break;

            case PivotAxis.YZ:
                directionToTarget.y = 0.0f;
                break;

            case PivotAxis.Free:
            default:
                // No changes needed.
                break;
        }

        // If we are right next to the camera the rotation is undefined. 
        if (directionToTarget.sqrMagnitude < 0.001f) {
            return;
        }

        // Calculate and apply the rotation required to reorient the object
        transform.rotation = Quaternion.LookRotation(-directionToTarget, targetUpVector);

    }
}
