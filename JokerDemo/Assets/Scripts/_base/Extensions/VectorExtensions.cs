﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorExtensions {

    public static Vector3 FindCenter(this Vector3[] v3s) {

        Vector3 total = Vector3.zero;

        foreach (Vector3 v3 in v3s) {
            total += v3;
        }

        return total / v3s.Length;

    }

    public static Vector3 RotatePointAroundPivot(this Vector3 point, Vector3 pivot, Vector3 angles) {

        return Quaternion.Euler(angles) * (point - pivot) + pivot;
    }
}
