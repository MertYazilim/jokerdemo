﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

/// <summary>
/// Extension methods for Unity's GameObject class
/// </summary>
public static class GameObjectExtensions
{
	public static T InstantiatePrefab<T>(this T source, Transform parent = null) where T : Object {
		T unit =
#if UNITY_EDITOR
			null;
		if (!Application.isPlaying)
			unit = PrefabUtility.InstantiatePrefab(source, parent) as T;
		else
#endif
			GameObject.Instantiate(source, parent);
		return unit;
	}

	public static void DontDestroyOnLoad(this Object target) {
#if UNITY_EDITOR // Skip Don't Destroy On Load when editor isn't playing so test runner passes.
        if (UnityEditor.EditorApplication.isPlaying)
#endif
            Object.DontDestroyOnLoad(target);
    }

    public static void Destroy(this GameObject go) {

        if(go != null) {
#if UNITY_EDITOR
            if (Application.isEditor) {
                GameObject.DestroyImmediate(go);
            }
            else
#endif
            {
                GameObject.Destroy(go);
            }
        }

    }


    /// <summary>
    /// Gets the GameObject's root Parent object.
    /// </summary>
    /// <param name="child">The GameObject we're trying to find the root parent for.</param>
    /// <returns>The Root parent GameObject.</returns>
    public static GameObject GetParentRoot(this GameObject child) {
        if (child.transform.parent == null) {
            return child;
        }
        else {
            return GetParentRoot(child.transform.parent.gameObject);
        }
    }

	public static void SetLayer(this GameObject gameObject, int layer) {
		gameObject.layer = layer;
	}

	public static void SetLayerRecursively(this GameObject gameObject, int layer) {
		gameObject.layer = layer;
		foreach (Transform child in gameObject.transform)
			SetLayerRecursively(child.gameObject, layer);
	}

}