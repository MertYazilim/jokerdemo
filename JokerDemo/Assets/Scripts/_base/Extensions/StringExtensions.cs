﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringExtensions {

    public static string TLSymbol(this string str) {

        return "₺ " + str;
    }
	
}
