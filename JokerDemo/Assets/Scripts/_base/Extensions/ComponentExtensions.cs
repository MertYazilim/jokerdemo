﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ComponentExtensions{


	public static T[] GetComponentsInMainChildren<T>(this Component component) where T : Component {

		Transform transform = component.transform;

		List<T> comps = new List<T>();
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			T t = child.GetComponent<T>();
			if (t != null) {
				comps.Add(t);
			}
		}
		return comps.ToArray();
	}

	public static T GetComponentInMainChildren<T>(this Component component) where T : Component{

        Transform transform = component.transform;

        for(int i = 0; i < transform.childCount; i++) {
            Transform child = transform.GetChild(i);
            T t = child.GetComponent<T>();
            if (t != null)
                return t;
        }
        return null;
    }

    public static T EnsureComponent<T>(this Component component) where T : Component {
        T t = component.GetComponent<T>();
        if(t == null) {
            t = component.gameObject.AddComponent<T>();
        }
        return t;
    }
 }
