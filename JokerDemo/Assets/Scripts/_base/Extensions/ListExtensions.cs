﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions {

	public static List<T> Copy<T>(this List<T> list) {
        List<T> newList = new List<T>();
        for (int i = 0; i < list.Count; i++)
            newList.Add(list[i]);
        return newList;
    }
}
