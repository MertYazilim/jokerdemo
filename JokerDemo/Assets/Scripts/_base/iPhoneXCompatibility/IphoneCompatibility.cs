﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IphoneCompatibility : MonoBehaviour {

    public float topMargin;

	private void Start () {
        if (ThisPlatform.IsIphoneX) {
            RectTransform rect = gameObject.GetComponent<RectTransform>();
            rect.offsetMax = new Vector2(rect.offsetMax.x, (topMargin * -1));
        }
	}
}
