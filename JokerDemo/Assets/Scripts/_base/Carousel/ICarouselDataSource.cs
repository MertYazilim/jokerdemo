﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICarouselDataSource<T> {

    List<T> GetCarouselDataSource();

}
