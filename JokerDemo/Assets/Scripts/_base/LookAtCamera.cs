﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {

    public Camera cameraToLookAt;
    [HideInInspector]
    public bool disableRotating = false;

    private void Start() {
        if (cameraToLookAt == null) {
            cameraToLookAt = Camera.main;
        }
    }

    void Update() {
        if (!disableRotating) {
            
            Vector3 v = cameraToLookAt.transform.position - transform.position;
            v.x = v.z = 0.0f;
            transform.LookAt(cameraToLookAt.transform.position - v);
            transform.Rotate(0, 180, 0);
        }
    }
}