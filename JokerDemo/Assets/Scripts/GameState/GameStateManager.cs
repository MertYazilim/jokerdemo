﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : SingleInstance<GameStateManager>
{
    private GameState _currentGameState = GameState.Welcome;
    public GameState CurrentGameState
    {
        get { return _currentGameState; }
        private set
        {
            if (value == _currentGameState)
            {
                return;
            }

            _currentGameState = value;
            OnGameStateChanged(_currentGameState);
        }
    }

    public Action<GameState> OnGameStateChanged;

    public void ChangeGameState(GameState state)
    {
        CurrentGameState = state;
    }

}
