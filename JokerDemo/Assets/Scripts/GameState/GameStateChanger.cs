﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateChanger : MonoBehaviour
{

    [SerializeField]
    private GameState _targetState;


    public void ChangeGameState()
    {
        StartCoroutine(ChangeGameStateNextFrame());   
    }

    private IEnumerator ChangeGameStateNextFrame()
    {
        yield return new WaitForEndOfFrame();

        GameStateManager.Instance.ChangeGameState(_targetState);
    }
}
