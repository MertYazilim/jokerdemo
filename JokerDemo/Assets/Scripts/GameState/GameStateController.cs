﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour
{

    [SerializeField]
    private GameState _targetState;
    [SerializeField]
    private GameObject _container;

    private void Awake()
    {
        GameStateManager.Instance.OnGameStateChanged += HandleGameStateChange;

        HandleGameStateChange(GameStateManager.Instance.CurrentGameState);
    }

    private void OnDestroy()
    {
        if (GameStateManager.Instance != null)
            GameStateManager.Instance.OnGameStateChanged -= HandleGameStateChange;
    }

    private void HandleGameStateChange(GameState newState)
    {
        _container.SetActive(_targetState == newState);
    }

}
