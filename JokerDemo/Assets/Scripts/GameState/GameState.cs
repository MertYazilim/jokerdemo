﻿public enum GameState
{
    Welcome,
    Shuffle,
    StartingGame,
    PlayerTurn,
    PCTurn,
    CalculatingScore,
    WaitingForANewGame
}