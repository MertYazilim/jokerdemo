﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardContainer : MonoBehaviour
{
    [HideInInspector]
    public List<Transform> slotList = new List<Transform>();
    [HideInInspector]
    public List<Vector3> positions = new List<Vector3>();

    private void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform slotTransform = transform.GetChild(i);
            slotList.Add(slotTransform);
            positions.Add(slotTransform.position);
        }
    }

    
}
