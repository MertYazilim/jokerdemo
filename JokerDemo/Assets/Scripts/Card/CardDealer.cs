﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDealer : MonoBehaviour
{
    
    [SerializeField]
    private CardObject _cardPrefab;
    [SerializeField]
    private CardHolder _cardHolder;
    [SerializeField]
    private Transform _cardSpawnPoint;
    [SerializeField]
    private GameStateChanger _endGameStateChanger;
    [SerializeField]
    private GameStateChanger _beginGameStateChanger;
    [SerializeField]
    private CardContainer _playerCardContainer;
    [SerializeField]
    private CardContainer _pcCardContainer;
    [SerializeField]
    private PCController _pcController;

    private int _dealedCardNumber = 0;
    private MidStackController _midStackController;

    private void Awake()
    {
        GameStateManager.Instance.OnGameStateChanged += HandleGameStateChange;
        
        _midStackController = MidStackController.Instance;
    }

    private void OnDestroy()
    {
        if(GameStateManager.Instance != null)
            GameStateManager.Instance.OnGameStateChanged -= HandleGameStateChange;
    }

    private void HandleGameStateChange(GameState newState)
    {
        if (newState == GameState.StartingGame)
        {
            SetupGame();
        }
    }

    private void SetupGame()
    {
        MidStackController.Instance.PushToStack(DealCards(MidStackController.Instance.GetPositions(4), 3, false));
        DealCardToPlayer();
        DealCardToPC();
        
        _beginGameStateChanger.ChangeGameState();
    }

    public List<CardObject> DealCards(List<Vector3> positions, int? closeCardNumber, bool isPlayerCard)
    {
        List<CardObject> cards = GetCards(positions.Count);

        for (int i = 0; i < cards.Count; i++)
        {
            CardObject card = cards[i];
            card.TargetPosition = positions[i];
            card.isPlayerCard = isPlayerCard;

            if (closeCardNumber.Value != null)
            {
                if (i < closeCardNumber.Value)
                {
                    card.IsCardOpen = false;
                }
                else
                {
                    card.IsCardOpen = true;
                }
            }
        }

        return cards;
    }

    private List<CardObject> GetCards(int numberOfCards)
    {
        CheckEndGame();

        List<Card> cards = _cardHolder.GetCards(numberOfCards);
        List<CardObject> cardObjects = new List<CardObject>();

        for (int i = 0; i < cards.Count; i++)
        {
            CardObject card = Instantiate(_cardPrefab, _cardSpawnPoint.localPosition, _cardSpawnPoint.localRotation);
            card.CardData = cards[i];

            cardObjects.Add(card);
        }

        _dealedCardNumber += numberOfCards;

        return cardObjects;
    }

    private void CheckEndGame()
    {
        if (_dealedCardNumber >= 52)
        {
            Debug.Log("GAME OVER");

            _endGameStateChanger.ChangeGameState();

            return;
        }
    }

    public void DealCardToPlayer()
    {
        DealCards(_playerCardContainer.positions, 0, true);
    }

    public void DealCardToPC()
    {
        _pcController.AddToHand(DealCards(_pcCardContainer.positions, 4, false));
    }
}
