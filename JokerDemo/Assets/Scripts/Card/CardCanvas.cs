﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardCanvas : MonoBehaviour 
{

    [SerializeField]
    private Image rightKindImage;
    [SerializeField]
    private Image leftKindImage;
    [SerializeField]
    private TextMeshProUGUI midValueText;
    [SerializeField]
    private TextMeshProUGUI rightValueText;
    [SerializeField]
    private TextMeshProUGUI leftValueText;
    [SerializeField]
    private GameObject _back;

    public void SetCardCanvas(CardKind kind, string visibleValue, Color valueColor)
    {
        Sprite kindIcon = Resources.Load<Sprite>(kind.ToString());
        if (kindIcon != null)
        {
            rightKindImage.sprite = kindIcon;
            leftKindImage.sprite = kindIcon;
        }

        midValueText.text = visibleValue;
        midValueText.color = valueColor;
        rightValueText.text = visibleValue;
        rightValueText.color = valueColor;
        leftValueText.text = visibleValue;
        leftValueText.color = valueColor;
    }

    public void ShowCardFront(bool value) {
        _back.SetActive(!value);
    }

}
