﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidStackController : SingleInstance<MidStackController>
{
    private List<CardObject> _stack = new List<CardObject>();

    public void PushToStack(CardObject card)
    {
        _stack.Add(card);
    }
    
    public void PushToStack(List<CardObject> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            PushToStack(cards[i]);
        }
    }

    public List<Vector3> GetPositions(int numberOfPositions)
    {
        List<Vector3> positions = new List<Vector3>();

        for (int i = 0; i < numberOfPositions; i++)
        {
            positions.Add(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), -((_stack.Count + i) / 100f)));
        }

        return positions;
    }

    public Card GetTopCardInfo()
    {
        return _stack[_stack.Count - 1].CardData;
    }
}
