﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardRequester : MonoBehaviour
{
    [SerializeField]
    private CardDealer _cardDealer;

    private int _turnCounter = 0;
    public int TurnCounter
    {
        get { return _turnCounter; }
        private set
        {
            _turnCounter = value;

            if (_turnCounter >= 9)
            {
                Debug.Log("9 turns.....");
                _cardDealer.DealCardToPlayer();
                _cardDealer.DealCardToPC();

                _turnCounter = 1;
            }
        }
    }

    private void Awake()
    {
        GameStateManager.Instance.OnGameStateChanged += HandleGameStateChanged;
    }

    private void OnDestroy()
    {
        if (GameStateManager.Instance != null)
            GameStateManager.Instance.OnGameStateChanged -= HandleGameStateChanged;
    }

    private void HandleGameStateChanged(GameState newState)
    {
        if (newState == GameState.PlayerTurn || newState == GameState.PCTurn)
        {
            TurnCounter++;
        }
    }
}
