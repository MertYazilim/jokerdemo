﻿public enum CardKind
{
    Spade,
    Club,
    Diamond,
    Heart
}
