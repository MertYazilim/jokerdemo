﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CardObject : MonoBehaviour
{
    private Card _cardData;
    public Card CardData
    {
        get
        {
            return _cardData;
        }
        set
        {
            _cardData = value;

            cardCanvas.SetCardCanvas(_cardData.kind, _cardData.visibleValue, _cardData.valueColor);
        }
    }

    public CardCanvas cardCanvas;

    private Vector3 _targetPosition;
    public Vector3 TargetPosition
    {
        get { return _targetPosition; }
        set
        {
            _targetPosition = value;

            transform.DOKill();
            transform.DOMove(_targetPosition, 1f, false).SetEase(Ease.OutSine);
        }
    }

    private bool _isCardOpen = false;
    public bool IsCardOpen
    {
        get { return _isCardOpen; }
        set
        {
            _isCardOpen = value;

            cardCanvas.ShowCardFront(_isCardOpen);
        }
    }

    [HideInInspector]
    public bool isPlayerCard = false;

}
