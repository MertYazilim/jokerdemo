﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardHolder : MonoBehaviour
{
    [SerializeField]
    private GameStateChanger _gameStateChanger;

    private List<Card> _cards = new List<Card>();

    private void Awake()
    {
        GameStateManager.Instance.OnGameStateChanged += HandleGameStateChange;
    }

    private void Ondestroy()
    {
        if (GameStateManager.Instance != null)
            GameStateManager.Instance.OnGameStateChanged += HandleGameStateChange;
    }

    public void PopulateCards() {
        for (int i = 0; i < System.Enum.GetValues(typeof(CardKind)).Length; i++)
        {
            CardKind kind = (CardKind)i;

            for(int j = 0; j < System.Enum.GetValues(typeof(CardValue)).Length; j++) {

                Card card = new Card(kind, (CardValue)j);
                _cards.Add(card);
            }
        }

        _gameStateChanger.ChangeGameState();
    }

    public List<Card> GetCards(int cardNumber)
    {
        List<Card> cards = new List<Card>();

        do
        {
            int randomIndex = (int)Random.Range(0f, (float)_cards.Count);
            Card randomCard = _cards[randomIndex];

            if (randomCard != null)
            {
                _cards.RemoveAt(randomIndex);
                cards.Add(randomCard);
            }

        } while (cards.Count < cardNumber);

        return cards;
    }

    private void HandleGameStateChange(GameState newState)
    {
        if (newState == GameState.Shuffle)
        {
            PopulateCards();
        }
    }
}
