﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    public CardKind kind;
    public CardValue value;
    public string visibleValue;
    public Color valueColor;

    public Card(CardKind kind, CardValue value)
    {
        this.kind = kind;
        this.value = value;

        switch (kind)
        {
            case CardKind.Spade:
                valueColor = Color.black;
                break;
            case CardKind.Club:
                valueColor = Color.black;
                break;
            case CardKind.Diamond:
                valueColor = Color.red;
                break;
            case CardKind.Heart:
                valueColor = Color.red;
                break;
            default:
                break;
        }

        switch (value)
        {
            case CardValue.Ace:
                visibleValue = "A";
                break;
            case CardValue.King:
                visibleValue = "K";
                break;
            case CardValue.Queen:
                visibleValue = "Q";
                break;
            case CardValue.Jack:
                visibleValue = "J";
                break;
            case CardValue.Ten:
                visibleValue = "10";
                break;
            case CardValue.Nine:
                visibleValue = "9";
                break;
            case CardValue.Eight:
                visibleValue = "8";
                break;
            case CardValue.Seven:
                visibleValue = "7";
                break;
            case CardValue.Six:
                visibleValue = "6";
                break;
            case CardValue.Five:
                visibleValue = "5";
                break;
            case CardValue.Four:
                visibleValue = "4";
                break;
            case CardValue.Three:
                visibleValue = "3";
                break;
            case CardValue.Two:
                visibleValue = "2";
                break;
        }
    }
}
